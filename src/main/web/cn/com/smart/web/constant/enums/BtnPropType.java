package cn.com.smart.web.constant.enums;

import cn.com.smart.web.bean.LabelValue;
import com.mixsmart.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 自定义按钮类型，常量定义
 * @author lmq
 * @version 1.0 2015年9月9日
 * @since 1.0
 *
 */
public interface BtnPropType {

	enum SelectType {
		/**
		 * 不选择数据
		 */
		NONE(1,"none-selected", "不选择数据"),
		/**
		 * 只能选择一条数据
		 */
		ONE(2,"one-selected", "选择一条数据"),
		/**
		 * 选择多条数据
		 */
		MULTI(3,"multi-selected", "选择多条数据");
		private int index;
		private String value;
		private String text;
		private SelectType(int index,String value, String text) {
			this.index = index;
			this.value = value;
			this.text = text;
		}
		
		public String getValue(int index) {
			String valueTmp = null;
			for (SelectType selectType : SelectType.values()) {
				if(selectType.getIndex() == index) {
					valueTmp = selectType.getValue();
					break;
				}
			}
			return valueTmp;
		}

		/**
		 * 获取表单列表类型对象
		 * @param value 类型值
		 * @return 返回类型对象
		 */
		public static SelectType getObj(String value) {
			if(StringUtils.isEmpty(value)) {
				return null;
			}
			SelectType type = null;
			for(SelectType typeTmp : SelectType.values()) {
				if(typeTmp.getValue().equals(value)) {
					type = typeTmp;
					break;
				}
			}
			return type;
		}

		/**
		 * 获取选项列表
		 * @return 返回选项列表
		 */
		public static List<LabelValue> getOptions() {
			List<LabelValue> labelValues = new ArrayList<LabelValue>();
			for(SelectType typeTmp : SelectType.values()) {
				labelValues.add(new LabelValue(typeTmp.getText(), typeTmp.getValue()));
			}
			return labelValues;
		}

		
		public int getIndex() {
			return index;
		}
		public void setIndex(int index) {
			this.index = index;
		}
		public String getValue() {
			return value;
		}
		public void setValue(String value) {
			this.value = value;
		}

		public String getText() {
			return text;
		}

		public void setText(String text) {
			this.text = text;
		}
	}
	
	
	enum OpenStyle {
	    
	    /**
	     * 没有打开样式
	     */
	    NONE(0, "", "无"),
	    
		/**
		 * 弹出框
		 */
		OPEN_POP(1,"open-pop", "弹出框"),

		/**
		 * 在当前所在页面打开
		 */
		OPEN_SELF(2,"open-self", "列表所在页面打开"),
		/**
		 * 打开一个新的窗口
		 */
		OPEN_BLANK(3,"open-blank", "新的窗口中打开"),
		
		/**
		 * 打开一个新的Tab窗口
		 */
		OPEN_NEW_TAB(4,"open-new-tab", "新tab中打开"),

		OPEN_NEW_TAB_IFRAME(5, "open-new-tab-iframe", "新tab中用iframe方式打开"),

		OPEN_IFRAME_POPUP(5, "open-iframe-popup", "iframe方式弹出框");
		
		private int index;
		private String value;
		private String text;
		private OpenStyle(int index,String value, String text) {
			this.index = index;
			this.value = value;
			this.text = text;
		}
		
		public String getValue(int index) {
			String valueTmp = null;
			for (OpenStyle openStyle : OpenStyle.values()) {
				if(openStyle.getIndex() == index) {
					valueTmp = openStyle.getValue();
					break;
				}
			}
			return valueTmp;
		}
		
		/**
		 * 根据值获取OpenStyle对象
		 * @param value 值
		 * @return 返回OpenStyle对象
		 */
		public static OpenStyle getValue(String value) {
		    OpenStyle valueTmp = null;
            for (OpenStyle openStyle : OpenStyle.values()) {
                if(openStyle.getValue().equals(value)) {
                    valueTmp = openStyle;
                    break;
                }
            }
            return valueTmp;
        }

		/**
		 * 获取表单列表类型对象
		 * @param value 类型值
		 * @return 返回类型对象
		 */
		public static OpenStyle getObj(String value) {
			if(StringUtils.isEmpty(value)) {
				return null;
			}
			OpenStyle type = null;
			for(OpenStyle typeTmp : OpenStyle.values()) {
				if(typeTmp.getValue().equals(value)) {
					type = typeTmp;
					break;
				}
			}
			return type;
		}

		/**
		 * 获取选项列表
		 * @return 返回选项列表
		 */
		public static List<LabelValue> getOptions() {
			List<LabelValue> labelValues = new ArrayList<LabelValue>();
			for(OpenStyle typeTmp : OpenStyle.values()) {
				labelValues.add(new LabelValue(typeTmp.getText(), typeTmp.getValue()));
			}
			return labelValues;
		}
		
		public int getIndex() {
			return index;
		}
		public void setIndex(int index) {
			this.index = index;
		}
		public String getValue() {
			return value;
		}
		public void setValue(String value) {
			this.value = value;
		}

		public String getText() {
			return text;
		}

		public void setText(String text) {
			this.text = text;
		}
	}
	
}
