package cn.com.smart.form.bean.entity;

import com.mixsmart.enums.YesNoType;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * 表单属性
 * @author lmq
 *
 */
@Entity
@Table(name="t_form")
public class TForm extends BaseForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3981943781706566675L;

	//非持久化属性；不会和数据库表字段关联
	//保存表单时是否自动生成列表；如果修改是有自动生成，则会覆盖原来的列表
	private Integer isAutoCreateList = YesNoType.NO.getIndex();

	@Transient
	public Integer getIsAutoCreateList() {
		return isAutoCreateList;
	}

	public void setIsAutoCreateList(Integer isAutoCreateList) {
		this.isAutoCreateList = isAutoCreateList;
	}
}
