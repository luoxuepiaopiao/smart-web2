package cn.com.smart.form.enums;

import com.mixsmart.utils.StringUtils;

/**
 * 文本框插件类型
 */
public enum TextPluginType {

    /**
     * cnoj-input-tree -- 树形
     */
    TEXT_PLUGIN_TREE("cnoj-input-tree"),
    
    /**
     * cnoj-auto-complete -- 自动完成
     */
    TEXT_PLUGIN_AUTO_COMPLETE("cnoj-auto-complete"),
    
    /**
     * cnoj-auto-complete-relate -- 自动完成关联填充
     */
    TEXT_PLUGIN_AUTO_COMPLETE_RELATE("cnoj-auto-complete-relate"),
    
    /**
     * cnoj-input-select -- 下拉框
     */
    TEXT_PLUGIN_SELECT("cnoj-input-select"),
    
    /**
     * cnoj-input-select-relate -- 下拉框关联填充
     */
    TEXT_PLUGIN_SELECT_RELATE("cnoj-input-select-relate");
    
    private String value;

    private TextPluginType(String value) {
        this.value = value;
    }

    /**
     * 获取文本插件类型对象
     * @param value 类型值
     * @return 返回类型对象
     */
    public static TextPluginType getObj(String value) {
        if(StringUtils.isEmpty(value)) {
            return null;
        }
        TextPluginType type = null;
        for(TextPluginType pluginType : TextPluginType.values()) {
            if(pluginType.getValue().equals(value)) {
                type = pluginType;
                break;
            }
        }
        return type;
    }
    
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
    
}
